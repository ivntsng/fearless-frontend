window.addEventListener("DOMContentLoaded", async () => {
  const confUrl = "http://localhost:8000/api/locations/";

  try {
    const confResponse = await fetch(confUrl);

    if (!confResponse) {
      const html = alertUser();
      const alert = document.querySelector(".row");
      alert.innerHTML = html;
    } else {
      const data = await confResponse.json();

      const selectTag = document.getElementById("location");
      for (let place of data.locations) {
        const options = document.createElement("option");
        options.value = place.id;
        options.innerHTML = place.name;
        selectTag.appendChild(options);
        console.log(options);
      }

      const formTag = document.getElementById("create-conference-form");
      formTag.addEventListener("submit", async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            "Content-Type": "application/json",
          },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newConference = await response.json();
        }
      });
    }
  } catch (error) {
    console.log(error);
  }
});
